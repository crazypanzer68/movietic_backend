package project.movie_tic.util

import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers
import project.movie_tic.entity.*
import project.movie_tic.entity.dto.*

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapMovie(movie: Movie?): MovieDto
    fun mapMovie(movie: List<Movie?>): List<MovieDto>
    fun mapShowTime(showtime: ShowTime?): ShowTimeDto
    fun mapShowTime(showtime: List<ShowTime?>): List<ShowTimeDto>
    fun mapCinema(cinema: Cinema?): CinemaDto
    fun mapCinema(cinema: List<Cinema?>): List<CinemaDto>
    fun mapSeat(seat: Seat?): SeatDto
    fun mapSeat(seat: List<Seat?>): List<SeatDto>
    fun mapUser(user: List<User?>):List<UserDto>
    fun mapUser(user: User): UserDto
    @InheritInverseConfiguration
    fun mapUser(user: UserDto): User

    fun mapRegister(user: User): RegisterDto
    @InheritInverseConfiguration
    fun mapRegister(user: RegisterDto): User

    fun mapEdit(user: User): User
    fun mapBooking(booking: Booking):BookingDto
    fun mapBooking(booking: List<Booking>):List<BookingDto>

}