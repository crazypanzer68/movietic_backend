package project.movie_tic.service

import project.movie_tic.entity.Movie

interface MovieService{
    fun getMovie():List<Movie>
    fun getMovieById(movieId: Long):Movie
}