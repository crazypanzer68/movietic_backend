package project.movie_tic.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.movie_tic.dao.ShowTimeDao
import project.movie_tic.entity.ShowTime

@Service
class ShowTimeServiceImpl:ShowTimeService{
    override fun getAllShowTime(): List<ShowTime> {
        return showTimeDao.getAllShowtime()
    }

    override fun getShowTimeById(id: Long): ShowTime {
        return showTimeDao.getShowTimeById(id)
    }

    override fun getShowTimeByMovieId(id: Long): List<ShowTime> {
        return showTimeDao.getShowTimeByMovieId(id)
    }
    @Autowired
    lateinit var showTimeDao: ShowTimeDao
}