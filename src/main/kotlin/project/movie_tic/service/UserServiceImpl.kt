package project.movie_tic.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.movie_tic.dao.UserDao
import project.movie_tic.entity.User
import project.movie_tic.entity.dto.ChangePasswordDto
import project.movie_tic.entity.dto.LoginDto
import project.movie_tic.entity.dto.RegisterDto
import project.movie_tic.entity.dto.UserDto
import project.movie_tic.util.MapperUtil

@Service
class UserServiceImpl : UserService {
    override fun findUserById(userId: Long?): User {
        return userDao.findUserById(userId)
    }

    override fun getAllUser(): List<User> {
        return userDao.getAllUser()
    }

    override fun login(user: LoginDto): User {
        var userData = userDao.findUserByEmail(user.email)
        if (userData.password == user.password) {
            return userData
        } else {
            return error("Password is invalid")
        }
    }

    override fun chagePassword(user: ChangePasswordDto): User {
        val edit = RegisterDto(
                id = user.id,
                firstName = userDao.findUserById(user.id).firstName,
                lastName = userDao.findUserById(user.id).lastName,
                password = user.password,
                email = userDao.findUserById(user.id).email)
        val user = MapperUtil.INSTANCE.mapRegister(edit)
        return userDao.save(user)

    }

    override fun edit(user: UserDto): User {
        val edit = RegisterDto(
                id = user.id,
                firstName = user.firstName,
                lastName = user.lastName,
                password = userDao.findUserById(user.id).password,
                email = user.email)
        val user = MapperUtil.INSTANCE.mapRegister(edit)
        return userDao.save(user)

    }

    override fun save(user: RegisterDto): User {
        val user = MapperUtil.INSTANCE.mapRegister(user)
        return userDao.save(user)
    }

    @Autowired
    lateinit var userDao: UserDao
}