package project.movie_tic.service

import project.movie_tic.entity.User
import project.movie_tic.entity.dto.ChangePasswordDto
import project.movie_tic.entity.dto.LoginDto
import project.movie_tic.entity.dto.RegisterDto
import project.movie_tic.entity.dto.UserDto

interface UserService{
    fun save(user: RegisterDto): User
    fun edit(user: UserDto): User
    fun chagePassword(user: ChangePasswordDto): User
    fun login(user: LoginDto): User
    fun getAllUser(): List<User>
    fun findUserById(userId: Long?): User

}