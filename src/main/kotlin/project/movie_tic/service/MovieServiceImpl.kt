package project.movie_tic.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.movie_tic.dao.MovieDao
import project.movie_tic.entity.Movie

@Service
class MovieServiceImpl:MovieService{
    override fun getMovieById(movieId: Long): Movie {
        return movieDao.getMovieById(movieId)
    }

    override fun getMovie(): List<Movie> {
        return movieDao.getMovie()
    }
    @Autowired
    lateinit var movieDao: MovieDao
}