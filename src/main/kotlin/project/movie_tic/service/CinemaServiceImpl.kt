package project.movie_tic.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.movie_tic.dao.CinemaDao
import project.movie_tic.entity.Cinema

@Service
class CinemaServiceImpl:CinemaService{
    override fun getAllCinema(): List<Cinema> {
        return cinemaDao.getAllCinema()
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao
}