package project.movie_tic.service

import project.movie_tic.entity.ShowTime

interface ShowTimeService{
    fun getShowTimeByMovieId(id: Long): List<ShowTime>
    fun getShowTimeById(id: Long): ShowTime
    fun getAllShowTime(): List<ShowTime>

}