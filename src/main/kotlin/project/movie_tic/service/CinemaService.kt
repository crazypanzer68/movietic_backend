package project.movie_tic.service

import project.movie_tic.entity.Cinema

interface CinemaService {
    fun getAllCinema(): List<Cinema>
}