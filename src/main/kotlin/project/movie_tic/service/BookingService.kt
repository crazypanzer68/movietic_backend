package project.movie_tic.service

import project.movie_tic.entity.Booking

interface BookingService {
    fun addBooking(booking: Booking, showTimeId: Long, userId: Long): Booking
    fun getBooking(userId: Long): List<Booking>
}