package project.movie_tic.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import project.movie_tic.dao.BookingDao
import project.movie_tic.dao.SeatInCinemaDao
import project.movie_tic.dao.ShowTimeDao
import project.movie_tic.dao.UserDao
import project.movie_tic.entity.Booking
import project.movie_tic.entity.SeatInCinema

@Service
class BookingServiceImpl : BookingService {

    override fun getBooking(userId: Long): List<Booking> {
        var user = userDao.findUserById(userId)
        return bookingDao.getBooking(user)
    }

    override fun addBooking(booking: Booking, showTimeId: Long, userId: Long): Booking {
        var showTime = showTimeDao.getShowTimeById(showTimeId)
        var selectSeat = mutableListOf<SeatInCinema>()
        for (item in booking.seats) {
            var seat = seatInCinemaDao.findById(item.id)
            seat.seatStatus = true
            selectSeat.add(seatInCinemaDao.save(seat))
        }
        var booking = Booking()
        booking.showTime = showTime
        booking.seats = selectSeat
        booking.createDateTime = System.currentTimeMillis()
        booking.user = userDao.findUserById(userId)
        return bookingDao.save(booking)
    }

    @Autowired
    lateinit var bookingDao: BookingDao
    @Autowired
    lateinit var showTimeDao: ShowTimeDao
    @Autowired
    lateinit var seatInCinemaDao: SeatInCinemaDao
    @Autowired
    lateinit var userDao: UserDao
}