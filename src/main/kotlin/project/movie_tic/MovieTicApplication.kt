package project.movie_tic

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MovieTicApplication

fun main(args: Array<String>) {
    runApplication<MovieTicApplication>(*args)
}
