package project.movie_tic.entity

enum class SeatType {
    PREMIUM, DELUXE, STANDARD
}