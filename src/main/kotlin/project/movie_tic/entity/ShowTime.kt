package project.movie_tic.entity

import java.util.*
import javax.persistence.*

@Entity
data class ShowTime(var startDateTime: Long? = null,
                    var endDateTime: Long? = null,
                    var soundNsubtitile: SoundTrackAndSubtitle? = SoundTrackAndSubtitle.EN_TH) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    var movie: Movie? = null
    @OneToOne
    var cinema: Cinema? = null
    @OneToMany
    var seats = mutableListOf<SelectedSeat>()

    constructor(startDateTime: Long,
                endDateTime: Long,
                movie: Movie) :
            this(startDateTime, endDateTime) {
        this.movie = movie
    }
}