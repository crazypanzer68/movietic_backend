package project.movie_tic.entity

import javax.persistence.*

@Entity
data class Booking(var createDateTime: Long? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @ManyToOne
    var user: User? = null
    @ManyToMany
    var seats = mutableListOf<SeatInCinema>()
    @ManyToOne
    var showTime: ShowTime? = null
}