package project.movie_tic.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SeatInCinema(var seatStatus: Boolean? = false,
                        @Column(name = "SeatInCinema_rows")
                        var row: String? = null,
                        @Column(name = "SeatInCinema_columns")
                        var column: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}