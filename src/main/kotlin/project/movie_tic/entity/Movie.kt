package project.movie_tic.entity

import javax.persistence.*

@Entity
data class Movie(var name: String? = null,
                 var duration: Int? = null,
                 var image: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var showTimes = mutableListOf<ShowTime>()
}
