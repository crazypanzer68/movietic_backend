package project.movie_tic.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class User(var firstName: String? = null,
                var lastName: String? = null,
                var email: String? = null,
                var password: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}