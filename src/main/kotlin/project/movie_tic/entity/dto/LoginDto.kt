package project.movie_tic.entity.dto

data class LoginDto(
        var email: String? = null,
        var password: String? = null
)