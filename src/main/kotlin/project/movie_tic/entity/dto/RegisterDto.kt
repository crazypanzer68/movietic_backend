package project.movie_tic.entity.dto

data class RegisterDto(
        var id: Long? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var password: String? = null,
        var email: String? = null
)