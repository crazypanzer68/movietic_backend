package project.movie_tic.entity.dto

import org.springframework.data.annotation.CreatedDate
import project.movie_tic.entity.SeatInCinema
import project.movie_tic.entity.ShowTime

data class BookingDto(
        var showTime: BookingInShowTimeDto? = null,
        var createDateTime: Long? = null,
        var seats: List<SeatInCinema> = mutableListOf()
)