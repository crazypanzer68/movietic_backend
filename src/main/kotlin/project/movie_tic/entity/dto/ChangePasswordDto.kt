package project.movie_tic.entity.dto

data class ChangePasswordDto(
        var id: Long? = null,
        var password: String? = null
)