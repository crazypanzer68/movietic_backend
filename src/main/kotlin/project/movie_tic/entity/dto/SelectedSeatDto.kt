package project.movie_tic.entity.dto

import project.movie_tic.entity.Seat

data class SelectedSeatDto(
        var seat: Seat? = null
)