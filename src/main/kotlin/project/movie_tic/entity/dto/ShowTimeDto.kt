package project.movie_tic.entity.dto

import project.movie_tic.entity.SelectedSeat
import project.movie_tic.entity.SoundTrackAndSubtitle

data class ShowTimeDto(
        var id: Long? = null,
        var movie: MovieDto? = null,
        var soundTrackAndSubtitle: SoundTrackAndSubtitle? = SoundTrackAndSubtitle.EN_TH,
        var startDateTime: Long? = null,
        var endDateTime: Long? = null,
        var cinema: CinemaDto? = null,
        var seats: List<SelectedSeat> = mutableListOf()
)