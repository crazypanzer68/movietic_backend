package project.movie_tic.entity.dto

import project.movie_tic.entity.SoundTrackAndSubtitle

data class BookingInShowTimeDto(
        var id: Long? = null,
        var movie: MovieDto? = null,
        var soundTrackAndSubtitle: SoundTrackAndSubtitle? = SoundTrackAndSubtitle.EN_TH,
        var startDateTime: Long? = null,
        var endDateTime: Long? = null
)