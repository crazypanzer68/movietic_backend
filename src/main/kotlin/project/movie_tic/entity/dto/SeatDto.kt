package project.movie_tic.entity.dto

import project.movie_tic.entity.SeatType

data class SeatDto(var name: String? = null,
                   var price: Double? = null,
                   var type: SeatType? = SeatType.STANDARD,
                   var row: Int? = null,
                   var column: Int? = null)
