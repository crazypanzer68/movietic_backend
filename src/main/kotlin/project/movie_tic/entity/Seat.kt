package project.movie_tic.entity

import javax.persistence.*

@Entity
data class Seat(var name: String? = null,
                var price: Double? = null,
                var seatType: SeatType? = SeatType.STANDARD,
                @Column(name = "Seat_rows")
                var row: Int? = null,
                @Column(name = "Seat_columns")
                var column: Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var seats = mutableListOf<SeatInCinema>()
}