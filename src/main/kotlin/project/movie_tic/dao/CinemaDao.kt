package project.movie_tic.dao

import project.movie_tic.entity.Cinema

interface CinemaDao {
    fun getAllCinema(): List<Cinema>
}