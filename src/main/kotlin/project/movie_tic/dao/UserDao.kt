package project.movie_tic.dao

import project.movie_tic.entity.User


interface UserDao {
    fun save(user: User): User
    fun findUserById(id: Long?): User
    fun findUserByEmail(email: String? = null): User
    fun getAllUser(): List<User>
}