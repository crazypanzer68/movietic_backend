package project.movie_tic.dao

import project.movie_tic.entity.Movie


interface MovieDao {
    fun getMovie(): List<Movie>
    fun getMovieById(id: Long): Movie
}