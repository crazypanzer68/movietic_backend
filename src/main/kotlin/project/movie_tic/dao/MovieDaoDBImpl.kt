package project.movie_tic.dao


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.Movie
import project.movie_tic.repository.MovieRepository

@Profile("db")
@Repository
class MovieDaoDBImpl : MovieDao {
    override fun getMovieById(id: Long): Movie {
        return movieRepository.findMovieById(id)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository

    override fun getMovie(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }
}