package project.movie_tic.dao

import project.movie_tic.entity.SeatInCinema

interface SeatInCinemaDao {
    fun findById(id: Long?): SeatInCinema
    fun save(seat: SeatInCinema): SeatInCinema
}