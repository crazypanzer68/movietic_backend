package dv.project.movietic.dao

import project.movie_tic.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.dao.SeatDao

@Profile("db")
@Repository
class SeatDaoDBImpl : SeatDao {
    @Autowired
    lateinit var seatRepository: SeatRepository
}