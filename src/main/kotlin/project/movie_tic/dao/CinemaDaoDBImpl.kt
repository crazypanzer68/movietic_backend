package project.movie_tic.dao

import project.movie_tic.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.Cinema

@Profile("db")
@Repository
class CinemaDaoDBImpl : CinemaDao {
    override fun getAllCinema(): List<Cinema> {
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }

    @Autowired
    lateinit var cinemaRepository: CinemaRepository
}