package project.movie_tic.dao

import project.movie_tic.repository.SeatInCinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.SeatInCinema

@Profile("db")
@Repository
class SeatInCinemaDaoDBImpl : SeatInCinemaDao {
    override fun save(seat: SeatInCinema): SeatInCinema {
        return seatInCinemaRepository.save(seat)
    }

    override fun findById(id: Long?): SeatInCinema {
        return seatInCinemaRepository.findById(id!!).orElse(null)
    }

    @Autowired
    lateinit var seatInCinemaRepository: SeatInCinemaRepository
}