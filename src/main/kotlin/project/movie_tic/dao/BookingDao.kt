package project.movie_tic.dao

import project.movie_tic.entity.Booking
import project.movie_tic.entity.User

interface BookingDao {
    fun getBooking(user: User): List<Booking>
    fun save(book: Booking): Booking
}