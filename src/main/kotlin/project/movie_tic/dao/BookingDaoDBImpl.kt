package project.movie_tic.dao

import project.movie_tic.repository.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.Booking
import project.movie_tic.entity.User

@Profile("db")
@Repository
class BookingDaoDBImpl : BookingDao {
    override fun save(booking: Booking): Booking {
        return bookingRepository.save(booking)
    }

    override fun getBooking(user: User): List<Booking> {
        return bookingRepository.findBookingByUser_Id(user.id!!)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
}