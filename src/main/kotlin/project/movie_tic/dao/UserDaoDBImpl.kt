package project.movie_tic.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.User
import project.movie_tic.repository.UserRepository

@Profile("db")
@Repository
class UserDaoDBImpl : UserDao {
    override fun getAllUser(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    override fun findUserByEmail(email: String?): User {
        return userRepository.findUserByEmail(email)
    }

    override fun findUserById(id: Long?): User {
        return userRepository.findUserById(id)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    @Autowired
    lateinit var userRepository: UserRepository
}