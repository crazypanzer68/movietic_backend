package project.movie_tic.dao

import project.movie_tic.entity.ShowTime

interface ShowTimeDao {
    fun getShowTimeByMovieId(id: Long): List<ShowTime>
    fun getShowTimeById(id: Long): ShowTime
    fun getAllShowtime(): List<ShowTime>

}