package project.movie_tic.dao

import project.movie_tic.repository.ShowTimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import project.movie_tic.entity.ShowTime

@Profile("db")
@Repository
class ShowTimeDaoDBImpl : ShowTimeDao {
    override fun getAllShowtime(): List<ShowTime> {
        return showTimeRepository.findAll().filterIsInstance(ShowTime::class.java)
    }

    override fun getShowTimeById(id: Long): ShowTime {
        return showTimeRepository.findShowtimeById(id)
    }

    override fun getShowTimeByMovieId(id: Long): List<ShowTime> {
        return showTimeRepository.findByMovie_Id(id)
    }

    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
}