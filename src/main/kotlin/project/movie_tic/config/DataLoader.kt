package project.movie_tic.config

import project.movie_tic.entity.Movie
import project.movie_tic.repository.MovieRepository
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import java.io.IOException
import javax.transaction.Transactional

@Component
class DataLoader {
    @Value("{xlsxPath}")
    val xlsxPath: String? = null
    @Autowired
    lateinit var movieRepository: MovieRepository


    fun loadData() {
        createMovieData()
    }

    fun getCellData(row: Row, col: String): String {
        val colIdx = CellReference.convertColStringToIndex(col)
        return getCellData(row, colIdx)
    }

    fun getCellData(row: Row, colIdx: Int): String {
        val cell = CellUtil.getCell(row, colIdx)
        if (cell.cellType == CellType.STRING) {
            if (cell.stringCellValue == "NULL")
                return ""
            return cell.stringCellValue
        } else if (cell.cellType == CellType.NUMERIC) {
            cell.cellType = CellType.STRING
            return cell.stringCellValue
        }
        return ""
    }

    //    @Transactional
//    fun createManufacturerData() {
//        try {
//            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
//            val workbook = XSSFWorkbook(file)
//            val sheet = workbook.getSheet("manufacturer")
//            val rowIterator = sheet.iterator()
//            rowIterator.next()
//            while (rowIterator.hasNext()) {
//                val row = rowIterator.next()
//                if (getCellData(row, "A") != "") {
//                    manufacturerRepository.save(Manufacturer(getCellData(row, "A"), getCellData(row, "B")))
//                }
//            }
//        } catch (e: IOException) {
//
//        }
//    }
    @Transactional
    fun createMovieData() {
        try {
            val file = xlsxPath?.let { ClassPathResource(it).inputStream }
            val workbook = XSSFWorkbook(file)
            val sheet = workbook.getSheet("movie")
            val rowIterator = sheet.iterator()
            rowIterator.next()
            while (rowIterator.hasNext()) {
                val row = rowIterator.next()
                if (getCellData(row, "A") != "") {
                    movieRepository.save(Movie(name = getCellData(row, "A"),
                            duration = getCellData(row, "B").toInt(),
                            image = getCellData(row, "C")))
                }
            }
        } catch (e: IOException) {

        }
    }
}