package project.movie_tic.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import project.movie_tic.entity.*
import project.movie_tic.repository.*
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var selectedSeatRepository: SelectedSeatRepository
    @Autowired
    lateinit var seatInCinemaRepository: SeatInCinemaRepository
    @Autowired
    lateinit var showTimeRepository: ShowTimeRepository
    @Autowired
    lateinit var dataLoader: DataLoader

    @Transactional
    override fun run(args: ApplicationArguments?) {
        val Day: Int = 86400000

        var standard = seatRepository.save(Seat("Standard", 150.00, SeatType.STANDARD, 4, 20))
        var deluxe = seatRepository.save(Seat("Deluxe", 190.00, SeatType.DELUXE, 10, 20))
        var premium = seatRepository.save(Seat("Premium(Pair)", 500.00, SeatType.PREMIUM, 1, 6))

        var cinema1 = cinemaRepository.save(Cinema("Cinema 1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema 2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema 3"))
        var cinema4 = cinemaRepository.save(Cinema("Cinema 4"))
        var cinema5 = cinemaRepository.save(Cinema("Cinema 5"))
        var cinema6 = cinemaRepository.save(Cinema("Cinema 6"))
        var cinema7 = cinemaRepository.save(Cinema("Cinema 7"))
        var cinema8 = cinemaRepository.save(Cinema("Cinema 8"))
        var cinema9 = cinemaRepository.save(Cinema("Cinema 9"))

        cinema1.seats = mutableListOf(premium, deluxe, standard)
        cinema2.seats = mutableListOf(premium, deluxe)
        cinema3.seats = mutableListOf(premium, deluxe, standard)
        cinema4.seats = mutableListOf(premium, deluxe, standard)
        cinema5.seats = mutableListOf(premium, deluxe, standard)
        cinema6.seats = mutableListOf(premium, deluxe)
        cinema7.seats = mutableListOf(premium, deluxe, standard)
        cinema8.seats = mutableListOf(premium, deluxe)

        var movies1 = movieRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล", 125, "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        var showtime1_1 = showTimeRepository.save(ShowTime(1553491800000 + Day, 1553491800000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime1_2 = showTimeRepository.save(ShowTime(1553493000000 + Day, 1553493000000 + Day + 7500000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime1_3 = showTimeRepository.save(ShowTime(1553499900000 + Day, 1553499900000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime1_4 = showTimeRepository.save(ShowTime(1553501100000 + Day, 1553501100000 + Day + 7500000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime1_5 = showTimeRepository.save(ShowTime(1553508000000 + Day, 1553508000000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime1_6 = showTimeRepository.save(ShowTime(1553509200000 + Day, 1553509200000 + Day + 7500000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime1_7 = showTimeRepository.save(ShowTime(1553516100000 + Day, 1553516100000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime1_8 = showTimeRepository.save(ShowTime(1553524200000 + Day, 1553524200000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))

        movies1.showTimes.add(showtime1_1)
        showtime1_1.movie = movies1
        showtime1_1.cinema = cinema1
        movies1.showTimes.add(showtime1_2)
        showtime1_2.movie = movies1
        showtime1_2.cinema = cinema2
        movies1.showTimes.add(showtime1_3)
        showtime1_3.movie = movies1
        showtime1_3.cinema = cinema1
        movies1.showTimes.add(showtime1_4)
        showtime1_4.movie = movies1
        showtime1_4.cinema = cinema2
        movies1.showTimes.add(showtime1_5)
        showtime1_5.movie = movies1
        showtime1_5.cinema = cinema1
        movies1.showTimes.add(showtime1_6)
        showtime1_6.movie = movies1
        showtime1_6.cinema = cinema2
        movies1.showTimes.add(showtime1_7)
        showtime1_7.movie = movies1
        showtime1_7.cinema = cinema1
        movies1.showTimes.add(showtime1_8)
        showtime1_8.movie = movies1
        showtime1_8.cinema = cinema1

        var movies2 = movieRepository.save(Movie("กัปตัน มาร์เวล", 125, "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        var showtime2_1 = showTimeRepository.save(ShowTime(1553491800000 + Day, 1553491800000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_2 = showTimeRepository.save(ShowTime(1553499900000 + Day, 1553499900000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_3 = showTimeRepository.save(ShowTime(1553508000000 + Day, 1553508000000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_4 = showTimeRepository.save(ShowTime(1553516100000 + Day, 1553516100000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_5 = showTimeRepository.save(ShowTime(1553524200000 + Day, 1553524200000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_6 = showTimeRepository.save(ShowTime(1553493000000 + Day, 1553493000000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_7 = showTimeRepository.save(ShowTime(1553501100000 + Day, 1553501100000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_8 = showTimeRepository.save(ShowTime(1553509200000 + Day, 1553509200000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_9 = showTimeRepository.save(ShowTime(1553517300000 + Day, 1553517300000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))
        var showtime2_10 = showTimeRepository.save(ShowTime(1553525400000 + Day, 1553525400000 + Day + 7500000, SoundTrackAndSubtitle.EN_TH))

        movies2.showTimes.add(showtime2_1)
        showtime2_1.movie = movies2
        showtime2_1.cinema = cinema3
        movies2.showTimes.add(showtime2_2)
        showtime2_2.movie = movies2
        showtime2_2.cinema = cinema3
        movies2.showTimes.add(showtime2_3)
        showtime2_3.movie = movies2
        showtime2_3.cinema = cinema3
        movies2.showTimes.add(showtime2_4)
        showtime2_4.movie = movies2
        showtime2_4.cinema = cinema3
        movies2.showTimes.add(showtime2_5)
        showtime2_5.movie = movies2
        showtime2_5.cinema = cinema3
        movies2.showTimes.add(showtime2_6)
        showtime2_6.movie = movies2
        showtime2_6.cinema = cinema4
        movies2.showTimes.add(showtime2_7)
        showtime2_7.movie = movies2
        showtime2_7.cinema = cinema4
        movies2.showTimes.add(showtime2_8)
        showtime2_8.movie = movies2
        showtime2_8.cinema = cinema4
        movies2.showTimes.add(showtime2_9)
        showtime2_9.movie = movies2
        showtime2_9.cinema = cinema4
        movies2.showTimes.add(showtime2_10)
        showtime2_10.movie = movies2
        showtime2_10.cinema = cinema4

        var movies3 = movieRepository.save(Movie("ชาแซม!", 135, "https://lh3.googleusercontent.com/gaToaVcS_L0LvoVdxAvdNvlm1u1-nKd03k0FhiY1t2pqzNBq0LjCEdXw8_Bumu8v7S7gdCtaf5awZUseZJ2K=w1024"))
        var showtime3_1 = showTimeRepository.save(ShowTime(1553493000000 + Day, 1553493000000 + Day + 8100000, SoundTrackAndSubtitle.EN_TH))
        var showtime3_2 = showTimeRepository.save(ShowTime(1553501700000 + Day, 1553501700000 + Day + 8100000, SoundTrackAndSubtitle.EN_TH))
        var showtime3_3 = showTimeRepository.save(ShowTime(1553510400000 + Day, 1553510400000 + Day + 8100000, SoundTrackAndSubtitle.EN_TH))
        var showtime3_4 = showTimeRepository.save(ShowTime(1553519100000 + Day, 1553519100000 + Day + 8100000, SoundTrackAndSubtitle.EN_TH))
        var showtime3_5 = showTimeRepository.save(ShowTime(1553527800000 + Day, 1553527800000 + Day + 8100000, SoundTrackAndSubtitle.EN_TH))

        movies3.showTimes.add(showtime3_1)
        showtime3_1.movie = movies3
        showtime3_1.cinema = cinema5
        movies3.showTimes.add(showtime3_2)
        showtime3_2.movie = movies3
        showtime3_2.cinema = cinema5
        movies3.showTimes.add(showtime3_3)
        showtime3_3.movie = movies3
        showtime3_3.cinema = cinema5
        movies3.showTimes.add(showtime3_4)
        showtime3_4.movie = movies3
        showtime3_4.cinema = cinema5
        movies3.showTimes.add(showtime3_5)
        showtime3_5.movie = movies3
        showtime3_5.cinema = cinema5

        var movies4 = movieRepository.save(Movie("ดัมโบ้", 105, "https://lh3.googleusercontent.com/FcKcdorsSc0A1gcPJ6Vgwjd1oVU0wgZP6m6rvOEj-INliNOeILq-ND3pR41N2RZCCP5scslco2shHqaVIq7c=w1024"))
        var showtime4_1 = showTimeRepository.save(ShowTime(1553493000000 + Day, 1553493000000 + Day + 6900000, SoundTrackAndSubtitle.EN_TH))
        var showtime4_2 = showTimeRepository.save(ShowTime(1553499900000 + Day, 1553499900000 + Day + 6900000, SoundTrackAndSubtitle.EN_TH))
        var showtime4_3 = showTimeRepository.save(ShowTime(1553506800000 + Day, 1553506800000 + Day + 6900000, SoundTrackAndSubtitle.EN_TH))
        var showtime4_4 = showTimeRepository.save(ShowTime(1553513700000 + Day, 1553513700000 + Day + 6900000, SoundTrackAndSubtitle.EN_TH))
        var showtime4_5 = showTimeRepository.save(ShowTime(1553520600000 + Day, 1553520600000 + Day + 6900000, SoundTrackAndSubtitle.EN_TH))

        movies4.showTimes.add(showtime4_1)
        showtime4_1.movie = movies4
        showtime4_1.cinema = cinema6
        movies4.showTimes.add(showtime4_2)
        showtime4_2.movie = movies4
        showtime4_2.cinema = cinema6
        movies4.showTimes.add(showtime4_3)
        showtime4_3.movie = movies4
        showtime4_3.cinema = cinema6
        movies4.showTimes.add(showtime4_4)
        showtime4_4.movie = movies4
        showtime4_4.cinema = cinema6
        movies4.showTimes.add(showtime4_5)
        showtime4_5.movie = movies4
        showtime4_5.cinema = cinema6

        var movies5 = movieRepository.save(Movie("หนุ่มน้อยสู่จอมราชันย์", 120, "https://lh3.googleusercontent.com/HFfLEJM3UranRaJTy0HNa1IJQ3oGNQHtX2BTMYj7obPczafPTN5AR4LgnIuzzmEYgKH8m1MBbp1tKUG-TzWT=w1024"))
        var showtime5_1 = showTimeRepository.save(ShowTime(1553491800000 + Day, 1553491800000 + Day + 7200000, SoundTrackAndSubtitle.EN_TH))
        var showtime5_2 = showTimeRepository.save(ShowTime(1553499900000 + Day, 1553499900000 + Day + 7200000, SoundTrackAndSubtitle.EN_TH))
        var showtime5_3 = showTimeRepository.save(ShowTime(1553508000000 + Day, 1553508000000 + Day + 7200000, SoundTrackAndSubtitle.EN_TH))
        var showtime5_4 = showTimeRepository.save(ShowTime(1553516100000 + Day, 1553516100000 + Day + 7200000, SoundTrackAndSubtitle.EN_TH))
        var showtime5_5 = showTimeRepository.save(ShowTime(1553493000000 + Day, 1553493000000 + Day + 7200000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime5_6 = showTimeRepository.save(ShowTime(1553501100000 + Day, 1553501100000 + Day + 7200000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime5_7 = showTimeRepository.save(ShowTime(1553509200000 + Day, 1553509200000 + Day + 7200000, SoundTrackAndSubtitle.TH_NOPE))
        var showtime5_8 = showTimeRepository.save(ShowTime(1553509200000 + Day, 1553509200000 + Day + 7200000, SoundTrackAndSubtitle.TH_NOPE))

        movies5.showTimes.add(showtime5_1)
        showtime5_1.movie = movies5
        showtime5_1.cinema = cinema7
        movies5.showTimes.add(showtime5_2)
        showtime5_2.movie = movies5
        showtime5_2.cinema = cinema7
        movies5.showTimes.add(showtime5_3)
        showtime5_3.movie = movies5
        showtime5_3.cinema = cinema7
        movies5.showTimes.add(showtime5_4)
        showtime5_4.movie = movies5
        showtime5_4.cinema = cinema7
        movies5.showTimes.add(showtime5_5)
        showtime5_5.movie = movies5
        showtime5_5.cinema = cinema8
        movies5.showTimes.add(showtime5_6)
        showtime5_6.movie = movies5
        showtime5_6.cinema = cinema8
        movies5.showTimes.add(showtime5_7)
        showtime5_7.movie = movies5
        showtime5_7.cinema = cinema8
        movies5.showTimes.add(showtime5_8)
        showtime5_8.movie = movies5
        showtime5_8.cinema = cinema8


/////////////////////////////showtime1_1/////////////////////////////////
        var row: Char = 'A'
        for ((index, item) in showtime1_1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_1.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
/////////////////////////////showtime1_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_2.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }

        /////////////////////////////showtime1_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_3.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_4.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_4.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_5.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_5.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_6.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_6.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_7/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_7.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_7.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime1_8/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime1_8.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1_8.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }

        /////////////////////////////showtime2_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_1.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_2.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_3.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_4.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_4.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_5.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_5.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_6.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_6.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_7/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_7.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_7.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_8/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_8.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_8.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_9/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_9.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_9.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime2_10/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime2_10.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2_10.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_1.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_2.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_3.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_4.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_4.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime3_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime3_5.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3_5.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_1.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_2.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_3.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_4.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_4.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime4_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime4_5.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4_5.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_1/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_1.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_2/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_2.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }

        /////////////////////////////showtime5_3/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_3.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_4/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_4.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_4.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_5/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_5.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_5.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_6/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_6.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_6.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_7/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_7.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_7.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
        /////////////////////////////showtime5_8/////////////////////////////////
        row = 'A'
        for ((index, item) in showtime5_8.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRepository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.column!!) {
                    var seat: SeatInCinema
                    if (item.seatType === SeatType.PREMIUM) {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInCinemaRepository.save(SeatInCinema(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5_8.seats.add(selectedSeat)
            if (item.seatType === SeatType.PREMIUM) {
                row = 'A'
            }
        }
    }
}
