package project.movie_tic.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import project.movie_tic.service.MovieService
import project.movie_tic.service.ShowTimeService
import project.movie_tic.util.MapperUtil

@RestController
class ShowTimeController {
    @Autowired
    lateinit var showTimeService: ShowTimeService
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/showTime")
    fun getAllShowTime(): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShowTime(showTimeService.getAllShowTime()))
    }

    @GetMapping("/showTime/movieId/{movieId}")
    fun getShowTimeByMovieId(@PathVariable movieId: Long): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShowTime(
                showTimeService.getShowTimeByMovieId(movieId)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/showTime/{showTimeId}")
    fun getShowTimeById(@PathVariable showTimeId: Long): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapShowTime(
                showTimeService.getShowTimeById(showTimeId)
        )
        return ResponseEntity.ok(output)
    }
}