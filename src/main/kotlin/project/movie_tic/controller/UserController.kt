package project.movie_tic.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import project.movie_tic.entity.dto.ChangePasswordDto
import project.movie_tic.entity.dto.LoginDto
import project.movie_tic.entity.dto.RegisterDto
import project.movie_tic.entity.dto.UserDto
import project.movie_tic.repository.UserRepository
import project.movie_tic.service.UserService
import project.movie_tic.util.MapperUtil

@RestController
class UserController {
    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user")
    fun getAllUser():ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.getAllUser()))
    }

    @GetMapping("/user/{userId}")
    fun getUserId(@PathVariable userId:Long?):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.findUserById(userId)))
    }

    @PostMapping("/user/login")
    fun login(@RequestBody user: LoginDto): ResponseEntity<Any> {
        if (user.email == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Email is not null")
        else if (user.password == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Password is not null")
        val login = MapperUtil.INSTANCE.mapUser(userService.login(user))
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(login))
    }

    @PostMapping("/user/register")
    fun register(@RequestBody user: RegisterDto): ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapRegister(userService.save(user)))
    }

    @PutMapping("/user/edit")
    fun edit(@RequestBody user: UserDto): ResponseEntity<Any> {
        if (user.id == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Id must not be null")
        }
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.edit(user)))
    }

    @PutMapping("/user/changePassword")
    fun changePassword(@RequestBody user: ChangePasswordDto): ResponseEntity<Any> {
        if (user.id == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("id must not be null")
        }
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.chagePassword(user)))
    }
}