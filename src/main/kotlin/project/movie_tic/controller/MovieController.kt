package project.movie_tic.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import project.movie_tic.service.MovieService
import project.movie_tic.util.MapperUtil

@RestController
class MovieController {
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movie")
    fun getAllMovie(): ResponseEntity<Any> {
        val movie = movieService.getMovie()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapMovie(movie))
    }

    @GetMapping("/movie/{movieId}")
    fun getMovieShowTime(@PathVariable movieId: Long): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapMovie(movieService.getMovieById(movieId)
        )
        return ResponseEntity.ok(output)
    }
}