package project.movie_tic.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import project.movie_tic.entity.Booking
import project.movie_tic.service.BookingService
import project.movie_tic.util.MapperUtil

@RestController
class BookingController {
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking/{showTimeId}/{userId}")
    fun addBooking(@RequestBody booking: Booking,
                   @PathVariable showTimeId: Long,
                   @PathVariable userId: Long): ResponseEntity<Any> {
        val bookingTicket = bookingService.addBooking(booking, showTimeId, userId)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBooking(bookingTicket))
    }

    @GetMapping("/booking/user/{userId}")
    fun getBooking(@PathVariable userId: Long): ResponseEntity<Any> {
        val ticket = bookingService.getBooking(userId)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBooking((ticket)))
    }
}