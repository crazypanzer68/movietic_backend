package project.movie_tic.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Repository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import project.movie_tic.service.CinemaService

@RestController
class CinemaController {
    @Autowired
    lateinit var cinemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema(): ResponseEntity<Any> {
        val output = cinemaService.getAllCinema()
        return ResponseEntity.ok(output)
    }
}