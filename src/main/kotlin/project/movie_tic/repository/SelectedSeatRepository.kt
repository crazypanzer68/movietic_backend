package project.movie_tic.repository

import org.springframework.data.repository.CrudRepository
import project.movie_tic.entity.SelectedSeat

interface SelectedSeatRepository : CrudRepository<SelectedSeat, Long>{

}