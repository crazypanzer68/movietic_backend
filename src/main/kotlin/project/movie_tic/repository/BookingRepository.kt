package project.movie_tic.repository

import project.movie_tic.entity.Booking
import org.springframework.data.repository.CrudRepository

interface BookingRepository : CrudRepository<Booking, Long> {
    fun findBookingByUser_Id(id: Long): List<Booking>
}