package project.movie_tic.repository

import project.movie_tic.entity.ShowTime
import org.springframework.data.repository.CrudRepository

interface ShowTimeRepository : CrudRepository<ShowTime, Long> {
    fun findByMovie_Id(id: Long): List<ShowTime>
    fun findShowtimeById(id: Long): ShowTime

}