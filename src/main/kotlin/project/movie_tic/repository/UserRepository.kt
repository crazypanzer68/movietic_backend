package project.movie_tic.repository

import org.springframework.data.repository.CrudRepository
import project.movie_tic.entity.User

interface UserRepository : CrudRepository<User, Long> {
    fun save(user: User): User
    fun findUserById(id: Long?): User
    fun findUserByEmail(email: String?): User
}