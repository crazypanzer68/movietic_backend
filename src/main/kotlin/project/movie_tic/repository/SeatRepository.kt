package project.movie_tic.repository

import project.movie_tic.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository : CrudRepository<Seat, Long> {

}