package project.movie_tic.repository

import project.movie_tic.entity.SeatInCinema
import org.springframework.data.repository.CrudRepository

interface SeatInCinemaRepository : CrudRepository<SeatInCinema, Long> {

}