package project.movie_tic.repository

import project.movie_tic.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository : CrudRepository<Cinema, Long>{

}