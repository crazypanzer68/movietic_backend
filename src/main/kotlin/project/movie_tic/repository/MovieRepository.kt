package project.movie_tic.repository

import project.movie_tic.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MovieRepository : CrudRepository<Movie, Long> {
    fun findMovieById(id: Long): Movie
}